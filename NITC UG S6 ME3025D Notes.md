# 2021-12-27-1300 IST V1.1

[Dynamical systems](https://mathworld.wolfram.com/DynamicalSystem.html) types:
  * Continuous time dynamical systems - governed by ODEs (form of $`\dot{x} = f(x); x_0 = c`$); denoted as flows in state/phase space.
  * Discrete time dynamical systems - governed by algebraic equations (form of $`x_{n + 1} = f(x_n)`$); denoted as [maps](https://mathworld.wolfram.com/Map.html), analogous to flows for continuous time dynamical systems described above. E.g., $`x_{n + 1} = 2x_n; x_0 = 0.3; x_0`$ is also called seed value. Then, $`x_1 = f(x_0) = 2x_0 = 0.6`$ and so on. Then, in general, $`x_n = 2^nx_0.`$ This example illustrates a *linear* map since the relation between the two variables is linear.

We are looking at the dynamics - the evolution of the variable with time/iterations, and the eventual pattern it may reach. We note that there is one variable in the above example - which is called a *one dimensional* map. A 2D map's example would be of the form: $`
\left\{
  \begin{array}{ll}
    x_{n + 1} &= f_1(x_n,y_n) \\
    y_{n + 1} &= f_2(x_n,y_n)
  \end{array}
\right.`$ <br>
It may be noted that all the systems discussed above are deterministic.

Consider $`x_{n + 1} = \cos x_n`$. We plot a graph of $`x_n`$ v. $`n`$, and see that the graph tends to a line as $`n \to \infty`$. Of course, it is not necessary that the function converge to a particular value always; it could diverge very rapidly as well, e.g., depending upon the seed value...

Consider $`x_{n + 1} = x_n^3.`$ [Orbit](https://mathworld.wolfram.com/MapOrbit.html) period is 1 if $`x_0 = 1.`$

Considering $`x_{n + 1} = x^2 - 1,`$ it may be observed that if $`x_0 = 1,`$ the following orbit results: $`1, 0, -1, 0, -1, 0, \dots`$ with orbit period 2.

# 2021-12-28-0900 IST V2.1

[Newton-Raphson method](https://mathworld.wolfram.com/NewtonsMethod.html): Numerical method used for finding root of algebraic equation $`y = f(x):D \mapsto C`$. The idea is to assume an arbitrary value $`x^* \in D`$ and draw a tangent to the curve at $`(x^*,f(x^*))`$ which meets $`XOX'`$ at $`g(x^*)`$. Then, $`f'(x^*) = \dfrac{f(x^*) - 0}{x^* - g(x^*)} \implies g(x^*) = x^* - \dfrac{f(x^*)}{f'(x^*)}`$. We then put $`x = g(x^*)`$ in RHS of $`g(x^*) = x^* - \dfrac{f(x^*)}{f'(x^*)}`$ to get (new) $`g(x^\dag),`$ and the process continues to get better estimates of the solution/root. Generalising, $`\boxed{g(x) = x - \dfrac{f(x)}{f'(x)}}.`$ <br>
E.g., consider $`f(x) = x^2 - 11.`$ We implement the above algorithm to get the closest numerical value of $`\sqrt{11}.\ \therefore\ x_{n + 1} = x_n - \dfrac{x_n^2 - 11}{2x_n}.`$ Applying $`6`$ iterations $`(x_0 = 1)`$, we find $`x = x_6 = 3.31662479 = \sqrt{11}.`$ The various values got in the intermediate iterations are given below: <br>
$`x_ 1 = 6 \\
x_2 = 3.91\bar{6} \\
x_3 = 3.362588652 \\
x_4 = 3.316938935 \\
x_5 = 3.316624805 \\
x_6 = 3.31662479 = x_i\ \forall\ i \in [7,\infty) \cap \mathbb{N}.`$ <br>
Indeed, $`\displaystyle \lim_{n \to \infty} x_n =
\left\{
  \begin{array}{ll}
    \sqrt{11}\ & \forall\ x_0 \in (0,\infty); \\
    -\sqrt{11}\ & \forall\ x_0 \in (-\infty,0)
  \end{array}
\right.`$

Another example - [Logistic map](https://mathworld.wolfram.com/LogisticMap.html): <br>
$`x_{n+1} = \mu x_n(1-x_n);\ \mu`$ is a constant parameter. <br>
The plot of $`x_n`$ v. $`n`$ shows erratic behaviour, i.e., randomness with no period. This aperiodicity is called 'chaos'.

Another example - [Tent map](https://en.wikipedia.org/wiki/Tent_map): <br>
$`f_\mu(x) : [0,1] \mapsto \left[0,\dfrac{\mu}{2}\right] := \mu\min \{x,1-x\}`$ (read as "function $`f`$ mapping \<domain\> to \<codomain\> defined as equal to \<expression\>") <br>
$`\implies x_{n + 1} = f_\mu (x_n) =
\left\{
  \begin{array}{ll}
    \mu x_n & \forall\ x_n \in \left[0,\dfrac{1}{2}\right]; \\
    \mu - \mu x_n & \forall\ x_n \in \left(\dfrac{1}{2},1\right].
  \end{array}
\right.`$

# 2021-12-30-1015 IST V3.1

[MATLAB](https://www.mathworks.com) code

```MATLAB
%Maps 1
%Generate a sequence of a given maps
%The user needs to input the initial value of the iterate 'a' and the number of iterations 'n'

clear %clears all the variables in memory
clf %clears all figures previously generated
%mu = 4; %a constant to be used in Logistic map if the logistic map is to be plotted
a = 0.4; %initial value of x, x_0 for the iteration
x(1) = a; %The zeroth value/seed value of the iterate, x_0 assigned to the first element of a one-dimensional array
n = 25; %Number of iterates in the sequence
for i = 2:n %Iteration loop starts
  x(i) = cos(a); %the map
  %x(i) = mu*a*(1 - a); %specifying mu for logistic map
  a = x(i); %assigning the new variable x_n for the next iteration
end %for every 'for', there is an 'end'
x %list the sequence of values of x(n), x_1 to the last value of x_n
%xlist = [x(1) x(10) x(100)] % list any specified values of x(n)
%plot(x,'o') %plots the iterates v. n, only the points
hold on %next plot will be superimposed on the current one
plot(x) %Plots the iterates v. number of iteration, points joined by lines
title('Figure 1 Iterates of the map x_{n+1} = cos(x_n)') %title of the figure
xlabel('n'), ylabel('x_n') %x and y labels for the axes
%title('Figure 2 Iterates of the Logistic map x_{n + 1} = mu*x_n*(1 - x_n)')
```

# 2021-12-30-1015 IST V3.2

[**Fixed point**](https://mathworld.wolfram.com/FixedPoint.html): Let $`f(x): D \mapsto C.\ x^* \in D \ni f(x^*) = x^* \implies x^*`$ is said to be a fixed point of $`f`$ (stationarity condition). <br>
E.g., consider $`x_{n + 1} = \cos x_n`$. To find the fixed point, we impose the condition $`x^* = \cos x^* \implies \cos x^* - x^* = 0 \implies x^*`$ is a zero of the function $`f(x) = \cos x - x`$. Thus, we can use the Newton-Rhapson method to find the said zero: $`g(x) = x - \dfrac{\cos x - x}{-\sin x - 1} = x + \dfrac{\cos x - x}{1 + \sin x}. \therefore`$, we get the final equation: <br>
$`x_{n + 1} = x_n + \dfrac{\cos x_n - x_n}{1 + \sin x_n}`$.
Taking $`x_0 = 0.4`$, we get: <br>
$`x_1 = 0.7750209552704571 \\
x_2 = 0.7393596756516518 \\
x_3 = 0.7390851498532536 \\
x_4 = 0.7390851332151607 = x_i\ \forall\ i \in [5,\infty) \cap \mathbb{N}`$. <br>
We observe that only $`4`$ iterations are needed for getting to the fixed point. Iterating normally/conventionally, <br>
$`x_0 = 0.4 \\
x_1 = 0.9210609940028851 \\
x_2 = 0.6049756872659429 \\
x_3 = 0.8225159255503863 \\
x_4 = 0.6803795415656718 \\
x_5 = 0.7773340096624639 \\
x_6 = 0.7127859455183532 \\
x_7 = 0.7565429619584524 \\
x_8 = 0.7272133023465764 \\
x_9 = 0.7470298705941069 \\
x_{10} = 0.7337101938664854 \\
\vdots \\
x_{24} = 0.7390638841882149 \\
\vdots \\
x_{91} = 0.7390851332151607 = x_i\ \forall\ i\ \in [92,\infty) \cap \mathbb{N}`$. <br>
We see that it took only 4 iterations for the fixed point value to be found using the Newton-Rhapson method, as opposed to the 92 iterations that it took using conventional method of using the original equation to find the fixed point to the same level of precision. This goes to show the power of the former method.

# 2022-12-30-1015 IST V3.3
The class was spent describing [cobweb diagrams](https://mathworld.wolfram.com/WebDiagram.html) through examples. An excellent interactive visualisation is given [here](https://www.desmos.com/calculator/unan9xh0og).

> * Write a program to generate a cobweb diagram.
> * Are there fixed points which attract from one side and repel to the other?
> * How will a periodic orbit P-2 look on a cobweb diagram?

# 2022-12-30-1015 IST V3.4
My definition of **Stable Fixed Point**: Consider $`f(x):D \mapsto C.\ x^* \in D`$ is said to be a stable fixed point/attractor if $`\forall\ x\in (x^*-\delta,x^*+\delta),\ \delta \to 0^+,\ \displaystyle\lim_{n\to\infty} \overset{n}{\underset{i=1}{\circ}} f(x) = x^*.`$ <br>
Consider $`f(x):D \mapsto C \ni f'(x) \in \mathbb{R}\ \forall\ x \in D.`$ Then, $`\left\|f'(x^*)\right\| \in
\left\{
  \begin{array}{ll}
    [0,1) \Longleftrightarrow x^* \text{ is stable} \\
    (1,\infty] \Longleftrightarrow x^* \text{ is unstable} \\
    \{1\}
  \end{array}
\right.`$ <br>
Below, we prove the first two parts of the above result. <br>
Let $`x^*`$ be a fixed point. Consider $`x := x^* + \delta_0`$ in the neighbourhood of $`x^*.`$ Then (applying Taylor series expansion), <br>
$`x^* + \delta_1 = f(x^* + \delta_0) = \displaystyle\sum_{r = 0}^\infty \frac{\delta_0^r}{r!}\left[\frac{\partial^r f(x)}{\partial x^r}\right]_{x\ =\ x^*} = f(x^*) + f'(x^*)\delta_0 + \mathcal{O}(x^*,\delta_0);\ \delta_1 \to 0^+ \\
\implies x^* + \delta_1 = x^* + f'(x^*)\delta_0 \implies \delta_1 \approx \lambda\delta_0;\ \lambda := f'(x^*).`$ In general, $`\delta_n \approx \lambda^n\delta_0. \\
\therefore n\to\infty \implies x^* + \delta_n \to
\left\{
  \begin{array}{ll}
    x^* \Longleftrightarrow \|\lambda\| \in [0,1); x^* \text{ is stable} \\
    \infty \Longleftrightarrow \|\lambda\| \in (1,\infty]; x^* \text{ is unstable} \\
    \text{//*To be investigated*// } \Longleftrightarrow \|\lambda\| \in\ \{1\};\ x^* \text{ is marginally/(neutrally?) stable}
  \end{array}
\right.`$ <br>
In general, in the aforementioned 2 cases, we say that the system is linearly (un)stable.

# 2021-01-10-1300 IST V4.1
Sometimes, the following terminology is used: $`\lambda \in
\left\{
  \begin{array}{ll}
    \mathbb{R} - \{1\} \implies \text{ \textquotedblleft hyperbolic"} \\
    \{1\} \implies \text{ \textquotedblleft non-hyperbolic"}
  \end{array}
\right.`$ <br>
Consider any function $`f`$ which touches $`y = x`$ without crossing over to the other side of the plane w.r.t. $`y = x.`$ <br>
[WOLOG](https://en.wikipedia.org/wiki/Without_loss_of_generality), let $`f(x): \displaystyle
\left\{
  \begin{array}{ll}
    \left(\mp\sqrt\dfrac{c}{a} - \dfrac{1}{a}, \infty\right) &; a \in \mathbb{R}^+ \\
    \left(-\infty, \mp\sqrt\dfrac{c}{a} - \dfrac{1}{a}\right) &; a \in \mathbb{R}^-
  \end{array}
\right\} \mapsto C := ax^2 + \left(1 \pm \sqrt{4ac}\right)x + c;\ ac \in [0,\infty)`$ We observe that $`y = f(x)`$ touches $`y = x`$ at $`x = \mp\sqrt\dfrac{c}{a},`$ and in no case crosses onto the other side of the plane vis-a-vis $`y = x.`$ Here, $`\mp\left(\sqrt\dfrac{c}{a}, \sqrt\dfrac{c}{a}\right)`$ is/are fixed point(s) for the function. On  examining the cobweb diagram for the given function, we observe that in any case, the fixed point is stable when approached from one side along $`XOX',`$ and unstable on being approached from the other side. <br>
Let us consider another function $`f(x):\mathbb{R} \mapsto \mathbb{R} := x^3 + x.`$ We observe that the fixed point is $`(0,0),`$ which also happens to be the point of inflection of the function. On examining the cobweb diagram, we observe that the fixed point is unstable $`\forall\ x \in \mathbb{R} - \{1\}.`$

On considering the dynamical system 

# 2021-01-10-1300 IST V4.2
We are also interested in the stability of periodic orbits. <br>
Consider an $`n`$-period orbit, with the orbit sequence being $`x_{1,n}, x_{2,n}, \dots, x_{n,n}`$ Applying the criterion for stability [above](#2022-12-30-1015-ist-v34), we get: <br>
$`\left\|\dfrac{d}{dx}\underset{i=1}{\overset{n}{\circ}} f(x)\right\|_{x=x_{p,n}} < 1;\ p\in [1,n] \cap \mathbb{N} \\
\implies \displaystyle \left\|\prod_{j=0}^{n-1} \left[\dfrac{d}{dx} f\left(\underset{i=1}{\overset{j}{\circ}} f(x)\right)\right]_{x=x_{p,n}}\right\| < 1 \\
\implies \left\|\prod_{j=0}^{n-1} f'\left(\underset{i=1}{\overset{j}{\circ}} f(x_{p,n}) \right) \right\| < 1`$ <br>
$`\implies \displaystyle \left\|\prod_{j=0}^{n-1} f'(x_{p+j,n})\right\| < 1 \\
\implies \left\|\prod_{q=1}^{n} f'(x_{q,n})\right\| < 1. \\
\therefore \left\|\prod_{q=1}^{n} f'(x_{q,n})\right\| \in
\left\{
  \begin{array}{ll}
    [0,1) \Longleftrightarrow \text{ Orbit is stable} \\
    (1,\infty) \Longleftrightarrow \text{ Orbit is unstable}
  \end{array}
\right.`$

**Eventually fixed point**: Let $`x \in D,\ f(x):D \mapsto C.`$ Then, $`\displaystyle \lim_{n\to\infty} \underset{i=1}{\overset{n}{\circ}} f(x) = x^* \implies x`$ is an eventually fixed point of $`f.`$ <br>
**Basin of attraction**: Let $`f(x):D \mapsto C,\ x^* \in D \ni x^* = f(x^*).`$ Then, the basin of attraction of $`x^*,\ B_{x^*} := \left\{x \in D : \displaystyle \lim_{n\to\infty} \underset{i=1}{\overset{n}{\circ}} f(x) = x^*\right\}.`$
