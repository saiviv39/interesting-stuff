# 2022-01-07-1115 IST

Robot specifications/characteristics:
  * Payload: Maximum load/weight robot can lift/withstand
  * Reach: When base stationary, maximum distance achieved by stretching
  * Degree of Freedom: No. of independent variables necessary for defining motion of mechanism
  * Repeatability: Ability to repeatedly reach target point/near it consistently
  * Accuracy: Closeness (of end-effector) to the actual desired value/position
  * Precision: "Measurement of Repeatability" //*Is it? Would say that this is more connected with resolution, if at all*//
  * Resolution: Smallest incremental movement that robot can perform in a controlled manner; normally a function of sensor used
  * Work-volume/Workspace: Locus of all points in space reached by end-effector
  * Drive-system/actuators - Hydraulics/mechanical/electromechanical
  * Controller - E.g., PID, etc.
  * Cost
  * Floor-space required

Selection of robots for a particular task may be based on:
  * Robot velocity: Velocity of the end-effector
  * Size of class -
    * Micro
    * Small
    * Medium
    * Large
  * Control mode -
    * Point-to-point control (PTP) [Pick and Place]
    * Continuous path control (CP) [Continuous weld]
    * Controlled path control [Control of velocity/acceleration]

General classification -
  1. Based on mechanical configuration - has cuboidal workspace
      * Cartesian configuration - LLL type $`\equiv`$ PPP type $`\equiv`$ 3P type (L: Linear; P: Prismatic)
      * Cylindrical configuration - has cylindrical workspace
        * RPP (Revolute Prismatic Prismatic)
        * RRP [e.g., SCARA (Selective compliance arm for robotic assembly) used for assembly]
      * Polar - has spherical workspace
        * TRP (Twisting Revolute Prismatic)
      * Joint-arm configuration - $`\sim`$ human arm configuration [PUMA: Programmable Universal Machine for Assembly - used for welding]

# 2022-01-10-0900 IST

Robot applications -
  * Material handling applications -
      * Loading and unloading machines
      * Presentation of parts for assembly operations
      * (De)palletising parts
  * Tool handling applications -
    * Machining operations - drilling, grounding, sanding, profiling, grinding
    * Finishing operations - spray painting, coating
    * Welding operations - Spot and continuous welding
  * Other tasks -
    * Assembly
    * Inspection
    * Data collection
  * Other special tasks -
    * Medical
    * Space
    * Military
    * Hazardous environments
    * Assistive technology

[Workcell](https://en.wikipedia.org/wiki/Workcell): A set of machines which focus on a specific process.

[Biomimetics](https://en.wikipedia.org/wiki/Biomimetics) <br>
Nanotechnology & Nanomedicine

# 2022-01-14-1115 IST
Roles of sensors: <br>
  * Detection - detects presence of external phenomenon
  * Selection - selects/filters out & measures property of the external stimulus
  * Signal processing - transforms input signal to a corresponding output signal in analog/digital/modulated form.
  * Communication - passes output signal to control system.

Information transfer path: environment $`to`$ sensor $`to`$ information/signal processing $`to`$ actuator $`to`$ environment (where we started in this info transfer path).
>Fuzzy logic controller works on the `if then` rule.

Factors considered for sensor-selection: <br>
  * Sensitivity: Ability of particular instruments to respond to changes $`:= \dfrac{\text{change in input}}{\text{change in output}}`$
  * Resolution
  * Precision
  * Repeatability - ability to get same measurement repeatedly for a constant quantity
  * Accuracy
  * Backlash - Maximum distance/angle through which part of mechanical system can move without causing motion (to system?)
  * Size
  * Cost
  * Availability
  * Weight
  * Power
  * Range
  * Linearity - variation of output with input (whether linear/non-linear)
  * (Ease of) Calibration: Compare performance of measuring device with a higher standard reference
  * Speed of response

Classification of sensors: <br>
  * General classification - contact/non-contact
  * Based on Signal characteristics - analog/digital
  * Based on Power supply
    * Active - Output produced from separate power source (e.g., LVDT, RVDT, potentiometers)
    * Passive - output is produced without separate power source (e.g., thermoelectric, piezoelectric, radioactive sensors)
  * Based on subject of measurement - mechanical/electrical/chemical/thermal/optical etc.
  * Mode of operation
    * Deflection - output $`\propto`$ quantity present/displayed
    * Null - deflection due to measurand quantity is balanced by opposing with a calibrated force

Robotic sensors - <br>
  * Tactile sensors - Identify shape of object as well as force required, etc.
    * Touch sensors (optical/mechanical) - limit switches, microswitches for interlock systems in robot, identifying the shapes of objects, inspection as probe, knowing the presence and absence of objects, etc.
    * Force/torque - Used for force, torque measurements at different locations like at joint, wrist, gripper, etc., of the robot
  * Position/range sensors - Used for measurements of displacements both rotary & linear as well as movements. Position information can be used for the measurements of velocities
  * Vision sensor - Inspection, identification, visual detection etc.

Image processing and analysis - consists of sense the appropriate parameter and then expressing it digitally (i.e., digitising it); //*difference between processing and analysis*//
  * Image processing: Convert the image so that it is ready for analysis (includes analog-digital conversion, discretisation, encoding, data reduction, segmentation, thresholding)
  * Image analysis: Feature/information extraction (object recognition, template comparison etc.)

Thereafter, we use the information got for application.

**Intelligence**: Ability to generate ordered output patterns in unpredictable situations; loosely, acting like humans in structured/unstructured environments

# 2022-01-17-0900 IST
**Simple serve system**:

Types of sensors -
  * Potentiometer sensor - variable resistance device used to measure linear and angular position; change in resistance is directly proportional to displacement using a variable resistor. Two types are rotary and linear. <br>
  $`V = \dfrac{Ed}{L}`$, where E: voltage across potentiometer, d is displacement, and L is the full scale displacement. Potentiometers are easy to use, low cost, high amplitude output, easily available, can be used to measuring even, size can be reduced significantly; disadvantages - low life cycle due to wear and tear due to sliding.
  * Capacitance sensor - Based on capacitance of capacitor, and the changes therein; particularly, by changing area and distance between the two plates; extremely sensitive, good frequency response, etc. Disadvantages include metallic parts must be insulated for reduction of effect of stray capacitances, non-linear behaviour sometimes on account of edge effects (guard ring used to eliminate this), cable connecting sensor to measuring point is also source of error. Applications include measurement of both linear and angular displacement, force & pressure, humidity in gases, etc.
  * Hall-effect sensor - relies on hall-effect - direction change of charge flow in perpendicular direction, leading to generation of voltage.
  * Piezo-electric sensor -
    * Logitudinal -
      * Direct effect sensor
      * Inverse effect actuators
    * Transverse
      * Direct effect sensor
      * Inverse effect actuators

    Tactile sensors use piezoelectric sensors; PVDF film - soft film - PVDF film. Soft film transfers the stresses to the lower film (?); and AC input is applied to the bottom film (PVDF), which causes oscillatory strain, which is transferred through the soft film onto the upper layer, because of which it will experiencing some force, with higher sensitivity (? Didn't understand at all)

# 2022-01-19-1015 IST
Temperature sensors -
  * Based on thermometric phenomenon
  * Based on thermoelectric phenomenon - thermistor, RTD (Resistance Temperature Detector; thermocouple are rarely used ?)
    * RTD - resistance changes (increases) with temperature $`[R_T = R_0(1 + \alpha\Delta T)]`$; metals used - Pt (very repeatable, quite sensitive, very expensive), Ni (not very repeatable, more sensitive, less expensive). Sensitivity depends upon $`\alpha;\ \alpha_{\text{Pt}} = 0.004\ \text{K}^{-1}`$ . Platinum RTD range: $`[-173.15,923.15] \text{K}`$. Advantages of RTDs include -
      * high sensitivity.... (document to be shared)
    * Thermocouple - two wires of different metal alloys; principle of operation is [Seebeck effect](https://en.wikipedia.org/wiki/Thermoelectric_effect#Seebeck_effect) (a combination of two other effects - [Thomson](https://en.wikipedia.org/wiki/Thermoelectric_effect#Thomson_effect) and [Peltier](https://en.wikipedia.org/wiki/Thermoelectric_effect#Peltier_effect) effects; the former being less significant than the latter); $`E = \alpha(T - T_0) + \beta (T - T_0)^2`$ (second order). <br>
    Advantages -
      * Simple, rugged
      * High temperature operations
      * low cost
      * No resistance lead wire problem
      * Point temperature sensing
      * Fastest response to temperature changes

      Disadvantages -
      * Least stable and repeatable
      * Low sensitivity to small temperature changes
      * Extension wire must be of the sane thermocouple type
      * Wire may pick up radiated electrical noise if not shielded
      * Lowest accuracy

    Chromel-constantin (upto 1273 K); Chromel-alumel combination (upto 1673 K); Pt-Ir combination (upto 1973 K) etc. For selection of thremocouple, know temperature range.
    * Thermistor - type of resistor used to measure temperature changes, relying on the change in its resistance with changing temperature. $`R_T = R_0e^{\beta\left(\frac{1}{T} - \frac{1}{T_0}\right)}`$ Beta function of temperature and property of semi-conductor material. <br>
    Advantages -
      * has high sensitivity and dynamic characteristics
      * construction in very small size.

      Disadvantages -
      * Non-linearity
      * Undesirable internal heating effects

Strain-gauges - used to measure strain of object; change of length implies change of resistance. Used in conjunction with Wheatstone bridge; temperature effects Gauge factor, $`\dfrac{\Delta R}{R} = \mathbf{G}\dfrac{\Delta L}{L} \implies \mathbf{G} = \dfrac{\Delta R/R}{\Delta L/L}.`$ Two types of strain-gauges -
  1. Bonded - Enclosed between two pieces of paper <br>
  Load-cell - measures force and torque as well; a combination of 4 strain-gauges which are attached on the curved surface of a cylinder with diametrically opposite gauges in the same orientation - horizontal or vertical.
  2. Unbonded - Not bounded (?)

  * Advantages - excellent accuracy and reliability
  * Disadvantages - low dynamic characteristics

Induction/proximity sensor - used for knowing position of metal depending upon measurement of magnetic field, without contact.

Tilt sensor - Used for measuring tilt/orientation when static.
  1. Electrolytic
  2. Bole (?) sensor

**Optical encoder**: A transducer commonly used for measuring rotational motion. It consists of a shaft connected to a circular disc, containing one or more tracks of alternating transparent and opaque areas. A light source and an optical sensor are mounted on opposite sides of each track. As the shaft rotates, the light sensor emits a series of pulses as the light source is interrupted by the pattern on the disc. This output signal can be directly compatible with digital circuitry. The number of output pulses per rotation of the disc is a known quantity, so the number of output pulses per second can be directly converted to the rotational speed (or rotations per second) of the shaft. Encoders are commonly used in motor speed control applications.[^1]

# 2022-01-21-1115 IST
Actuators - Hardware devices that convert controller command signal into change in physical parameter; is also a transducer that converts one form of energy into another. Electrical actuators for less loads; hydraulic actuators in high-load conditions. Important factors/parameters considered before selecting actuators -
  1. Cost
  2. Speed
  2. Power - required/output
  2. Resolution
  2. Repeatability <br>

  * DC motors used for high accuracy, high torque, good control. $`\tau = k_t I; V = k_v \omega`$; brush arcing and maintenance problem with conventional DC motors - hence using brushless motors.
  * AC motor - operate without brushes; popular in many machine tools; induction, synchronous, single and 3-phase motors classification. Advantages - inter-phaseability with AC signal to (?) <br>
  * Stepper and servomotor -
    * Stepper - translates electrical pulses into precise and equally spaced rotations; no feedback loop, i.e., open loop system. Stepper motor low torque, low speed, higher vibration. Consists of stator with pre-set number of poles.
      * Types - variable reluctance (stator windings are excited in sequence causing rotor to align to particular position; has higher torque) & permanent magnet stepper motors (has smaller steps, i.e., better resolution)
    * Servomotor - Closed loop system using optical encoder to detect output error, which is converted from digital to analogue signal which is then executed.
  * Pneumatic actuators - block diagram
  * Hydraulic actuators - driven by liquid, relatively compact; block diagram
  * Valves, their types and their symbols
  * Accumulators
  * Piezoelectric actuators

# 2022-01-24-0900 IST
Components of a manipulator robot -
  * Base
  * Joint
  * Link
  * End-effector

**End-effector**: Device attached to wrist/link at end of manipulator to enable general-purpose robot to perform specific task; also called robot 'hand'.

Types of end-effectors -
  * Grippers - Grasp and hold functions. Further classified as -
    * Internal grippers
    * External grippers
  * Tools - respective functions (e.g., welding)

Basic gripper design -
  * Motive is to transform power input to gripping action by fingers of object.
  * Types of power input -
    1. Electric
    2. Mechanical
    3. Hydraulic
    4. Pneumatic
  * Finger grasping design philosophy
    1. Contact surface customised to part shape or vice-versa (i.e., part made with specified gripping space of particular shape)
    2. Gripping using friction model - cheapest, less complicated, but also least customisability
    3. Hybrid of friction/magnetic/adhesion/inflatation etc.

The gripper must be responsive to external factors that might increase the force on the gripped object in the direction parallel to the contact plane(s) of the object and the fingers. This essentially means that the gripper should be able to dynamically resist relative movement of the object. For static equilibrium,
```math
\boxed{\mu n_f F_g = w} \dots (1) \\
\mu: \text{coefficient of friction} \\
n_f: \text{number of contacting fingers} \\
F_g: \text{gripper force} \\
w: \text{weight of gripped object}
```

However, we note that that is only when the robot is not accelerating. In cases where the acceleration of the robot is considerable, Eq(1) becomes <br>
$`\mu n_f F_g = wg,`$ <br>
where $`g`$ is a factor that takes into account the combined effect of gravity and acceleration.
  * Types of grippers -
    * Pivoting movement; linear/translational movement (similar to lathe chuck key grasping things)
    * Per kinematic mechanism used to actuate finger movement
      1. Linkage actuation
      2. Gear-and-rack actuation
      2. Cam actuation
      2. Screw actuation
      2. Rope-and-pulley actuation
    * Other types
      1. Vaccum cups - only one surface required for grasping; pneumatic suction used; simple
          * Doesn't work for porous and delicate surfaces
      2. Magnetic -
          * Pick up time is fast
          * Only for ferromagnetic materials
      3. Adhesive -
          * Light-weight, delicate materials only (fabrics, etc. without deformation on object)
          * Loss of tackiness after large number of cycles - continuous feeding of adhesive ribbons to offset that needed
      4. Inflatable -
          * Inflated after inserting (partially/fully) into a hole of diameter smaller than that of the fully inflated gripper
          * Light weight
          * Used in applications such as narrow-necked bottle-grabbing
  * Tools on end-effector
    * Welding end-effector - welding tool attached to end of manipulator
    * Spray painting end-effector.

//*Please go through force analysis of grippers in text-books.*//

[^1]: https://www.sciencedirect.com/topics/engineering/optical-encoders
